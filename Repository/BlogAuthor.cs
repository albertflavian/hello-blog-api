using System.Collections.Generic;

namespace hello_blog_api.Repository
{
    public class BlogAuthor
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public ICollection<BlogPost> BlogPosts{ get; set; }
    }
}