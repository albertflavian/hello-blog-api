using System.Collections.Generic;

namespace hello_blog_api.Repository
{
public class Tag
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<BlogPostTag> BlogPostTags{ get; set; }
    }
}