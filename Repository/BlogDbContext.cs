using Microsoft.EntityFrameworkCore;
using System;

namespace hello_blog_api.Repository
{
    public class BlogDbContext : DbContext
    {
        public string DbPath { get; }

        public BlogDbContext(DbContextOptions<BlogDbContext> options)
            : base(options)
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = System.IO.Path.Join(path, "blogging.db");
        }
 
        public DbSet<BlogPost> BlogPosts { get; set; }

        public DbSet<Tag> Tags {get; set; }

        public DbSet<BlogPostTag> BlogPostTags { get; set; }

        public DbSet<BlogAuthor> BlogAuthors { get; set; }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlogPostTag>()
                .HasKey(bc => new { bc.TagId, bc.BlogPostId });

            modelBuilder.Entity<BlogPostTag>()
                .HasOne(bc => bc.Tag)
                .WithMany(b => b.BlogPostTags)
                .HasForeignKey(bc => bc.TagId);
                
            modelBuilder.Entity<BlogPostTag>()
                .HasOne(bc => bc.BlogPost)
                .WithMany(c => c.BlogPostTags)
                .HasForeignKey(bc => bc.BlogPostId);

            modelBuilder.Entity<Tag>()
                .Property(t => t.Name)
                .IsRequired();

            modelBuilder.Entity<Tag>()
                .HasIndex(i => i.Name)
                .IsUnique();

            modelBuilder.Entity<BlogPost>()
                .HasOne(bc => bc.BlogAuthor)
                .WithMany(a => a.BlogPosts);

            modelBuilder.Entity<BlogAuthor>()
                .HasData(new BlogAuthor() { Id = 1, FirstName = "Bob", LastName = "The writer"});
        }
    }
}