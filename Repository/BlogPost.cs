using System.Collections.Generic;

namespace hello_blog_api.Repository
{
public class BlogPost
    {
        public int Id { get; set; }

        public string Label { get; set; }
        
        public string Title { get; set; }
        
        public string Content { get; set; }

        public ICollection<BlogPostTag> BlogPostTags{ get; set; }

        public int BlogAuthorId { get; set; }
        
        public BlogAuthor BlogAuthor { get; set; }
    }
}