namespace hello_blog_api.Repository
{
public class BlogPostTag
    {
        public int BlogPostId { get; set; }

        public int TagId { get; set; }

        public BlogPost BlogPost { get; set; }

        public Tag Tag { get; set; }
    }
}