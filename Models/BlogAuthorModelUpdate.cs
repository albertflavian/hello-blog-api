using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class BlogAuthorModelUpdate
    {   
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}