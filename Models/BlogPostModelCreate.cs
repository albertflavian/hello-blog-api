using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class BlogPostModelCreate
    {
        public BlogPostModelCreate()
        {
            Tags = new List<string>();
        }

        public string Label { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public List<string> Tags { get; set; }

        [Required]
        public int BlogAuthorId { get; set; }
    }
}