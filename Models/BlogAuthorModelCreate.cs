using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class BlogAuthorModelCreate
    {
        [Required]
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}