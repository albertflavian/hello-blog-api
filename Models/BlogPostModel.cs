using System.Collections.Generic;

namespace hello_blog_api.Models
{
    public class BlogPostModel
    {
        public BlogPostModel()
        {
            Tags = new List<string>();
        }
        
        public int Id { get; set; }

        public string Label { get; set; }
        
        public string Title { get; set; }
        
        public string Content { get; set; }

        public List<string> Tags { get; set; }

        public int BlogAuthorId { get; set; }

        public BlogAuthorModel BlogAuthor { get; set; }
    }
}