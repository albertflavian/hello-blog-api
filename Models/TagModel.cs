using System.Collections.Generic;

namespace hello_blog_api.Models
{
    public class TagModel
    {
        public TagModel()
        {
            BlogPostModels = new List<BlogPostModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BlogPostModel> BlogPostModels { get; set; }
    }
}