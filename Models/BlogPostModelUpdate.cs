using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace hello_blog_api.Models
{
    public class BlogPostModelUpdate
    {
        public BlogPostModelUpdate()
        {
            Tags = new List<string>();
        }

        [Required]
        public int Id { get; set; }
        
        public string Label { get; set; }
        
        [Required]
        public string Title { get; set; }
        
        [Required]
        public string Content { get; set; }

        public List<string> Tags { get; set; }

        [Required]
        public int BlogAuthorId { get; set; }
        
        public BlogAuthorModelCreate BlogAuthor { get; set; }
    }
}