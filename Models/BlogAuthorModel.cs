using System.Collections.Generic;

namespace hello_blog_api.Models
{
    public class BlogAuthorModel
    {
        public BlogAuthorModel()
        {
            BlogPosts = new List<BlogPostModel>();
        }
        public BlogAuthorModel(BlogAuthorModel blogAuthorModel)
        {
            Id = blogAuthorModel.Id;
            FirstName = blogAuthorModel.FirstName;
            LastName = blogAuthorModel.LastName;
            BlogPosts = new List<BlogPostModel>();
        }
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<BlogPostModel> BlogPosts{ get; set; }
    }
}