using AutoMapper;
using hello_blog_api.Models;
using hello_blog_api.Repository;
using System.Linq;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        // Configure two-way mapping between desired models
        CreateMap<BlogPost, BlogPostModel>()
              .ForMember(x => x.Tags, m => m.MapFrom(src => src.BlogPostTags.Select(t => t.Tag.Name).ToList()))
              .ReverseMap();

        CreateMap<BlogPost, BlogPostModelCreate>()
              .ForMember(x => x.Tags, m => m.MapFrom(src => src.BlogPostTags.Select(t => t.Tag.Name).ToList()))
              .ReverseMap();

        CreateMap<BlogPost, BlogPostModelUpdate>()
              .ForMember(x => x.Tags, m => m.MapFrom(src => src.BlogPostTags.Select(t => t.Tag.Name).ToList()))
              .ReverseMap();

        CreateMap<BlogAuthor, BlogAuthorModel>().ReverseMap();
        CreateMap<BlogAuthor, BlogAuthorModelCreate>().ReverseMap();
        CreateMap<BlogAuthor, BlogAuthorModelUpdate>().ReverseMap();

    }
}