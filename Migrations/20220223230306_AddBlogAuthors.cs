﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace hello_blog_api.Migrations
{
    public partial class AddBlogAuthors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BlogAuthorId",
                table: "BlogPosts",
                type: "INTEGER",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateTable(
                name: "BlogAuthors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(type: "TEXT", nullable: true),
                    LastName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogAuthors", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "BlogAuthors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { 1, "Bob", "The writer" });

            migrationBuilder.CreateIndex(
                name: "IX_BlogPosts_BlogAuthorId",
                table: "BlogPosts",
                column: "BlogAuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_BlogPosts_BlogAuthors_BlogAuthorId",
                table: "BlogPosts",
                column: "BlogAuthorId",
                principalTable: "BlogAuthors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlogPosts_BlogAuthors_BlogAuthorId",
                table: "BlogPosts");

            migrationBuilder.DropTable(
                name: "BlogAuthors");

            migrationBuilder.DropIndex(
                name: "IX_BlogPosts_BlogAuthorId",
                table: "BlogPosts");

            migrationBuilder.DropColumn(
                name: "BlogAuthorId",
                table: "BlogPosts");
        }
    }
}
