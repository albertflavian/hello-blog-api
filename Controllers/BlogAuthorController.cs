using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using System.Net;
using hello_blog_api.Repository;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogAuthorController : Controller
    {
        private readonly BlogDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public BlogAuthorController(BlogDbContext dbContext, IMapper mapper, ILogger<BlogPostController> logger)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
        }

        ///<summary>
        ///Creates a blog author using the payload information.
        ///Endpoint url: api/v1/BlogAuthor
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogAuthorModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateBlogAuthor([FromBody] BlogAuthorModelCreate blogAuthor)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Model is not valid!");
                }

                var entity = _mapper.Map<BlogAuthor>(blogAuthor);

                var createdEntity = await _dbContext.BlogAuthors.AddAsync(entity);

                var savedItemsCount = await _dbContext.SaveChangesAsync();

                if (savedItemsCount != 1)
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                }

                return CreatedAtAction(nameof(CreateBlogAuthor), _mapper.Map<BlogAuthorModel>(createdEntity.Entity));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog author by ID.
        ///Endpoint url: api/v1/BlogAuthor
        ///</summary>
        [HttpGet]
        [Route("GetById/{blogAuthorId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetBlogAuthorById(int blogAuthorId)
        {
            try
            {
                var blogAuthor = await _dbContext.BlogAuthors.SingleOrDefaultAsync(a => a.Id == blogAuthorId);

                if (blogAuthor == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }

                return Ok(_mapper.Map<BlogAuthorModel>(blogAuthor));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of authors with the associated blog posts filtered by name.
        ///Endpoint url: api/v1/BlogAuthor
        ///</summary>
        [HttpGet]
        [Route("SearchAuthors/{name}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> SearchAuthors(string name)
        {
            try
            {
                name = name.ToLower();
                var blogAuthors = await _dbContext.BlogAuthors
                                    .Include(b => b.BlogPosts)
                                    .ThenInclude(bp => bp.BlogPostTags)
                                    .ThenInclude(bpt => bpt.Tag)
                                    .Where(x => x.FirstName.ToLower().Contains(name) || x.LastName.ToLower().Contains(name)).ToListAsync();

                if (blogAuthors.Count == 0)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }

                return Ok(blogAuthors.Select(blogAuthor => _mapper.Map<BlogAuthorModel>(blogAuthor)));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of authors with the associated blog posts.
        ///Endpoint url: api/v1/BlogAuthor
        ///</summary>
        [HttpGet]
        [Route("GetAuthors")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAuthors()
        {
            try
            {
                var blogAuthors = await _dbContext.BlogAuthors
                                    .Include(b => b.BlogPosts)
                                    .ThenInclude(bp => bp.BlogPostTags)
                                    .ThenInclude(bpt => bpt.Tag)
                                    .ToListAsync();

                // Something is definetely wrong in this query
                // var blogAuthors = await (from blogAuthor in _dbContext.BlogAuthors
                //             join blogPost in _dbContext.BlogPosts on blogAuthor.Id equals blogPost.BlogAuthorId
                //             select new BlogAuthorModel(_mapper.Map<BlogAuthorModel>(blogAuthor)){
                //                 BlogPosts = new List<BlogPostModel>(){_mapper.Map<BlogPostModel>(blogPost)}
                //             }).ToListAsync();

                return Ok(blogAuthors.Select(blogAuthor => _mapper.Map<BlogAuthorModel>(blogAuthor)));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Updates a author.
        ///Endpoint url: api/v1/BlogAuthor
        ///</summary>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateBlogAuthor([FromBody] BlogAuthorModelUpdate blogAuthor)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Model is not valid!");
                }

                _dbContext.Update(_mapper.Map<BlogAuthor>(blogAuthor));

                await _dbContext.SaveChangesAsync();

                return Ok(blogAuthor);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }


        ///<summary>
        ///Delets a author based on the ID.
        ///Endpoint url: api/v1/BlogAuthor/{blogAuthorId}
        ///</summary>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogAuthorId}")]
        public async Task<IActionResult> DeleteAuthorById([FromRoute] int blogAuthorId)
        {
            try
            {
                var entity = await _dbContext.BlogAuthors.SingleAsync(x => x.Id == blogAuthorId);
                _dbContext.BlogAuthors.Remove(entity);

                var blogAuthorWasDeleted = (await _dbContext.SaveChangesAsync()) == 1;

                if (!blogAuthorWasDeleted)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }

                return Ok();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

    }
}