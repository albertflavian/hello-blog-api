using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using System.Net;
using System.Collections.Generic;
using hello_blog_api.Repository;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController : Controller
    {
        private readonly BlogDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public BlogPostController(BlogDbContext dbContext, IMapper mapper, ILogger<BlogPostController> logger)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
        }

        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateBlogPost([FromBody] BlogPostModelCreate blogPost)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Model is not valid!");
                }

                BlogPost entity = _mapper.Map<BlogPost>(blogPost);

                //var blogPostTags = blogPost.Tags.Select(tag => new BlogPostTag(){ BlogPost = entity, Tag = new Tag(){Name = tag}}).ToList();

                var blogPostTags = new List<BlogPostTag>();

                foreach (var tagName in blogPost.Tags)
                {
                    var tag = _dbContext.Tags.SingleOrDefault(t => t.Name == tagName);

                    if (tag == null)
                    {
                        tag = new Tag() { Name = tagName };
                        await _dbContext.Tags.AddAsync(tag);
                    }
                    blogPostTags.Add(new BlogPostTag() { BlogPost = entity, Tag = tag });
                }

                entity.BlogPostTags = blogPostTags;
                var createdEntity = await _dbContext.BlogPosts
                    .AddAsync(entity);

                var savedItemsCount = await _dbContext.SaveChangesAsync();

                if (savedItemsCount < 1)
                {
                    return StatusCode((int)HttpStatusCode.InternalServerError);
                }

                return CreatedAtAction(nameof(CreateBlogPost), _mapper.Map<BlogPostModel>(createdEntity.Entity));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of all blogs.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAllBlogPostsAsync()
        {
            try
            {
                // Mapare manuala
                // List<BlogPostModel> blogPosts = await _dbContext.BlogPosts
                //     .Select(x => new BlogPostModel(){ 
                //         Id = x.Id,
                //         Content = x.Content,
                //         Title = x.Title
                //         })
                //     .ToListAsync();

                // method syntax cu mapare automata cu automapper
                // List<BlogPostModel> blogPosts = await _dbContext.BlogPosts
                //     .Select(blogPost => _mapper.Map<BlogPostModel>(blogPost))
                //     .ToListAsync();

                // query syntax cu mapare automata cu automapper
                // List<BlogPostModel> blogPosts = await (
                //     from blogPost in _dbContext.BlogPosts
                //     join blogPostTags in _dbContext.BlogPostTags on blogPost.Id equals blogPostTags.BlogPostId into gt from blogPostTags in gt.DefaultIfEmpty()
                //     join tags in _dbContext.Tags on blogPostTags.TagId equals tags.Id  into g from tags in g.DefaultIfEmpty()
                //     select _mapper.Map<BlogPostModel>(blogPost))
                //     .ToListAsync();

                var blogPosts = await _dbContext.BlogPosts
                    .Include(a => a.BlogAuthor)
                    .Include(b => b.BlogPostTags)
                    .ThenInclude(bt => bt.Tag)
                    .ToListAsync();

                return Ok(blogPosts.Select(b => _mapper.Map<BlogPostModel>(b)));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public async Task<IActionResult> GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                var blogPost = await _dbContext.BlogPosts
                    .Include(a => a.BlogAuthor)
                    .Include(b => b.BlogPostTags)
                    .ThenInclude(bt => bt.Tag)
                    .SingleOrDefaultAsync(b => b.Id == blogPostId);

                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(_mapper.Map<BlogPostModel>(blogPost));
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of blogs based on a tag.
        ///Endpoint url: api/v1/BlogPost/Tag/{tag}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [Route("tag/{blogPostTag}")]
        public async Task<IActionResult> GetBlogPostByTag([FromRoute] string blogPostTag)
        {
            try
            {
                var blogPosts = await _dbContext.BlogPosts
                        .Include(a => a.BlogAuthor)
                        .Include(b => b.BlogPostTags)
                        .ThenInclude(bt => bt.Tag)
                        .Where(bp => bp.BlogPostTags.Any(t => t.Tag.Name == blogPostTag))
                        .Select(x => _mapper.Map<BlogPostModel>(x))
                        .ToListAsync();

                return Ok(blogPosts);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Updates a blog.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> UpdateBlogPost([FromBody] BlogPostModelUpdate blogPost)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Model is not valid!");
                }

                var entity = await _dbContext.BlogPosts
                    .Include(a => a.BlogAuthor)
                    .Include(b => b.BlogPostTags)
                    .ThenInclude(bt => bt.Tag)
                    .SingleOrDefaultAsync(b => b.Id == blogPost.Id);

                if (entity.BlogPostTags == null) entity.BlogPostTags = new List<BlogPostTag>();

                foreach (var blogPostTag in entity.BlogPostTags)
                {
                    if (!blogPost.Tags.Contains(blogPostTag.Tag.Name))
                    {
                        entity.BlogPostTags.Remove(blogPostTag);
                    }
                }

                foreach (var tagName in blogPost.Tags)
                {
                    if (entity.BlogPostTags.Any(t => t.Tag.Name == tagName)) { continue; }

                    var tag = _dbContext.Tags.SingleOrDefault(t => t.Name == tagName);

                    if (tag == null)
                    {
                        tag = new Tag() { Name = tagName };
                        await _dbContext.Tags.AddAsync(tag);
                    }
                    entity.BlogPostTags.Add(new BlogPostTag() { BlogPost = entity, Tag = tag });
                }

                entity.Content = blogPost.Content;
                entity.Label = blogPost.Label;
                entity.Title = blogPost.Title;

                await _dbContext.SaveChangesAsync();

                return Ok(_mapper.Map<BlogPostModel>(entity));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Delets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public async Task<IActionResult> DeleteBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                //var blogPostWasDeleted = BlogPostTestData.DeleteBlogPostById(blogPostId);

                var entity = await _dbContext.BlogPosts.SingleAsync(x => x.Id == blogPostId);
                _dbContext.BlogPosts.Remove(entity);

                var blogPostWasDeleted = (await _dbContext.SaveChangesAsync()) == 1;

                if (!blogPostWasDeleted)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }

                return Ok();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

    }
}