# Webinars - Build your own blog using the Microsoft tech stack

Detalii despre cele 8 sesiuni care se desfasoara in perioada 15 Februarie - 12 Aprilie 2022 gasiti mai jos

* [WebApi & REST](#webapi--rest)
* [Entity Framework](#entity-framework)

## Resurse necesare pentru a rula/testa codul și pentru a rezolva temele:

    VS Code : https://code.visualstudio.com/download 
    .Net 5.0 Framework : https://dotnet.microsoft.com/en-us/download/dotnet/5.0
    Git : https://git-scm.com/download/win

## WebApi & REST

### Speakers
    Daniel Cotoară
    Alina Hamza
    Dragoș Dumitriu

* Prezentare power point [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/325f5cb8e7fcf3caee06aba639d27f13/WebApi___REST.pptx)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-WebAPI&Rest-Webinar)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

## Entity Framework

### Speakers
    Liviu Popovici
    Marius Brînzea

* Prezentare power point [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/uploads/805b34e434472e0005c6023a550c7573/EntityFramework.pptx)
* Temele disponibile [aici](https://gitlab.com/csv-webinars/hello-blog-api/-/wikis/Teme-EntityFramework-Webinar)
* Pasi necesari pentru rezolvarea temelor [aici](#pa%C8%99i-necesari-pentru-rezolvarea-temelor)

### Comenzi folosite pe durata prezentarii
```console
# Instalare pachete nuget. 
# Parametrul version e necesar pentru ca ultima versiune a acestor pachete sunt compatibile doar cu proiecte .Net 6
dotnet add package Microsoft.EntityFrameworkCore.InMemory --version 5.0.14
dotnet add package AutoMapper --version 11.0.1
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection --version 11.0.0
dotnet add package Microsoft.EntityFrameworkCore.Sqlite --version 5.0.14

# Instalare suport entity framework pentru a rula comenzi din terminal
dotnet tool install --global dotnet-ef

# Adauga suport pt code-first migration
dotnet add package Microsoft.EntityFrameworkCore.Design --version 5.0.14

# Creeaza baza de data initiala
dotnet ef migrations add InitialCreate

# Executa migrarea pentru actualizare a bazei de date cu structura de date (tabele) conform claselor definite in proiect
# Pentru a migra modificarile ulterioare (noile DBSet-uri definite in cod) se ruleaza tot aceasta comanda
dotnet ef database update
```

#### Resetarea migrarilor deja efectuare si restaurarea structurii bazei de date la forma ei initiala (fara date)
```console
dotnet ef database update 0
dotnet ef database update
```

## Pași necesari pentru rezolvarea temelor 

 1) Creare unui cont pe [gitlab](https://docs.gitlab.com/ee/gitlab-basics/index.html).

 2) Fork la [repository](https://gitlab.com/csv-webinars/hello-blog-api) utilizănd [pașii](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).

 3) [Clonare](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) a repository-ului personal după fork.

 4) [Crearea](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#create-a-branch) unui branch (Opțional).

 5) Rezolvarea temei/temelor.

 6) [Commit](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-and-commit-local-changes) la modificări.

 7) [Push](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#send-changes-to-gitlabcom) la modificări în repository-ul personal.

 8) [Crearea](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-an-issue) unui Issue. Completați cămpurile după cum urmează:

        a) Title = Tema + Numele webinar-ului + Link-ul catre repository-ul personal care a fost creat dupa fork. 
            Exemplu -> Tema WebApi&Rest : Link https://gitlab.com/csv-webinars/hello-blog-api.git
        b) Type = Issue
        c) Description = O descriere a ce s-a rezolvat în respectiva temă. Exemplu -> Din cele 3 teme am rezolvat complet 2 dintre ele (Tema_1 si Tema_2) și am încercat să fac ceva și la Tema_3
        d) Assignees = Cel puțin o persoană dintre cei care au ținut respectivul webinar (se pot vedea în secțiunea : Speakers)
        d) Restul câmpurilor se pot lăsa cu valorile default.

## Instrucțiuni folositoare pentru lucrul cu Git
- [Learn git branching](https://learngitbranching.js.org/)

## Instrucțiuni folositoare în VS Code Terminal
- Build la proiect : dotnet build
- Start la proiect : dotnet run 
    - Url-ul unde va fi hostată aplicația va aparea în Terminal. Acesta poate fi vizibil și în fișierul : launchSettings.json
- Alte [instrucțiuni](https://git-scm.com/docs/git#_git_commands) utile

## Pentru orice probleme va rugăm sa faceți un Issue urmănd pașii de mai sus cu precizarea de a schimba: 

    a) Title = Problema
    b) Type = Incident
    c) Description = Descrierea problemei și opțional atașare de imagini dacă este nevoie.
